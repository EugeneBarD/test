﻿using System;

#pragma warning disable S2368

namespace LookingForArrayElements
{
    public static class DecimalCounter
    {
        /// <summary>
        /// Searches an array of decimals for elements that are in a specified range, and returns the number of occurrences of the elements that matches the range criteria.
        /// </summary>
        /// <param name="arrayToSearch">One-dimensional, zero-based <see cref="Array"/> of single-precision floating-point numbers.</param>
        /// <param name="ranges">One-dimensional, zero-based <see cref="Array"/> of range arrays.</param>
        /// <returns>The number of occurrences of the <see cref="Array"/> elements that match the range criteria.</returns>
        public static int GetDecimalsCount(decimal[] arrayToSearch, decimal[][] ranges)
        {
            if (arrayToSearch is null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch), $"{nameof(arrayToSearch)} is null!");
            }

            if (ranges is null)
            {
                throw new ArgumentNullException(nameof(ranges), $"{nameof(ranges)} is null!");
            }

            for (int k = 0; k < ranges.Length; k++)
            {
                if (ranges[k] is null)
                {
                    throw new ArgumentNullException(nameof(ranges), "There is range that is null!");
                }

                if (ranges[k] != Array.Empty<decimal>())
                {
                    if (ranges[k].Length != 2)
                    {
                        throw new ArgumentException("Length of one of ranges is less or greater than 2.");
                    }

                    if (ranges[k][0] > ranges[k][1])
                    {
                        throw new ArgumentException($"{nameof(ranges)} has a range in which 1 element is greater than 2 element");
                    }
                }
            }

            if (arrayToSearch.Length == 0 || ranges.Length == 0)
            {
                return 0;
            }

            int matchesCount = 0;
            int i = 0;
            int j = 0;

            do
            {
                do
                {
                    if (ranges[j].Length == 0)
                    {
                        j++;
                        continue;
                    }

                    if (arrayToSearch[i] >= ranges[j][0] && arrayToSearch[i] <= ranges[j][1])
                    {
                        matchesCount++;
                        break;
                    }

                    j++;
                }
                while (j < ranges.Length);

                j = 0;
                i++;
            }
            while (i < arrayToSearch.Length);

            return matchesCount;
        }

        /// <summary>
        /// Searches an array of decimals for elements that are in a specified range, and returns the number of occurrences of the elements that matches the range criteria.
        /// </summary>
        /// <param name="arrayToSearch">One-dimensional, zero-based <see cref="Array"/> of single-precision floating-point numbers.</param>
        /// <param name="ranges">One-dimensional, zero-based <see cref="Array"/> of range arrays.</param>
        /// <param name="startIndex">The zero-based starting index of the search.</param>
        /// <param name="count">The number of elements in the section to search.</param>
        /// <returns>The number of occurrences of the <see cref="Array"/> elements that match the range criteria.</returns>
        public static int GetDecimalsCount(decimal[] arrayToSearch, decimal[][] ranges, int startIndex, int count)
        {
            if (arrayToSearch is null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch), $"{nameof(arrayToSearch)} is null!");
            }

            if (ranges is null)
            {
                throw new ArgumentNullException(nameof(ranges), $"{nameof(ranges)} is null!");
            }

            for (int k = 0; k < ranges.Length; k++)
            {
                if (ranges[k] is null)
                {
                    throw new ArgumentNullException(nameof(ranges), "There is range that is null!");
                }

                if (ranges[k] != Array.Empty<decimal>())
                {
                    if (ranges[k].Length != 2)
                    {
                        throw new ArgumentException("Length of one of ranges is less or greater than 2.");
                    }

                    if (ranges[k][0] > ranges[k][1])
                    {
                        throw new ArgumentException($"{nameof(ranges)} has a range in which 1 element is greater than 2 element");
                    }
                }
            }

            if (startIndex < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex), $"{nameof(startIndex)} is less than zero");
            }

            if (startIndex > arrayToSearch.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex), $"{nameof(startIndex)} is greater than arrayToSearch.Length");
            }

            if (count < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(count), $"{nameof(count)} is less than zero");
            }

            if (startIndex + count > arrayToSearch.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(count), $"{nameof(startIndex)} + {nameof(count)} > arrayToSearch.Length");
            }

            int matchesCount = 0;

            for (int i = startIndex; i < startIndex + count; i++)
            {
                for (int j = 0; j < ranges.Length; j++)
                {
                    if (ranges[j].Length == 0)
                    {
                        j++;
                        continue;
                    }

                    if (arrayToSearch[i] >= ranges[j][0] && arrayToSearch[i] <= ranges[j][1])
                    {
                        matchesCount++;
                        break;
                    }
                }
            }

            return matchesCount;
        }
    }
}
